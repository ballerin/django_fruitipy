#!/bin/bash
python3 /usr/src/app/fruitipy_server/manage.py migrate
python manage.py syncdb --noinput
echo "from django.contrib.auth.models import User; User.objects.create_superuser('admin', 'admin@example.com', 'djangoadmin') if (not User.objects.filter(username='admin').exists()) else print('Admin already set')" | python3  /usr/src/app/fruitipy_server/manage.py shell
python3 /usr/src/app/fruitipy_server/manage.py runserver 0.0.0.0:8000
