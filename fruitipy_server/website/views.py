from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse
from django.shortcuts import render
from django.views.generic import ListView

from api.models import Sample, Image
from bokeh.plotting import figure
from bokeh.embed import components
from bokeh.resources import CDN


def Profile(request):
    if(request.user.is_superuser):
        # Superuser
        pass
    else:
        # Normal user
        pass

    return HttpResponse('<meta http-equiv="refresh" content="0; url=../../samples_list/">')



def spectrum_plot(request, id):

    spectrum_string = Sample.objects.get(pk=id).spectrum
    spectrum = spectrum_string.split(',')
    plot = figure(responsive=True)
    plot.line(range(288),spectrum)

    script, div = components(plot, CDN)

    return render(request, "spectrum_plot.html", {"the_script": script, "the_div": div})


class HistoryListView(LoginRequiredMixin, ListView):
    """
    Allows user to check and modify labels and validate the sample for further training.
    """

    template_name = "history_list.html"

    def get_queryset(self):
        # original = Sample.objects.get().filter(pk=req_id)
        history = Sample.objects.filter(uni_id=self.kwargs['req_id']).order_by('pk')
        return history


class SampleListView(LoginRequiredMixin, ListView):
    """
    Allows user to check and modify labels and validate the sample for further training.
    """
    paginate_by = 20
    def get_queryset(self):
        samples = Sample.objects.filter(uni_id=-1).order_by('pk')
        if (not self.request.user.is_superuser):
            samples = samples.filter(user=self.request.user)
        for s in samples:
            if s.gps is not None:
                if s.gps.find("/")!=-1:
                    s.lat = s.gps.split("/")[0]
                    s.lon = s.gps.split("/")[1]
                elif s.gps.find(",")!=-1:
                    s.lat = s.gps.split(",")[0]
                    s.lon = s.gps.split(",")[1]

        return samples

class ImageListView(LoginRequiredMixin, ListView):
    """
    Allows user to see images.
    """
    paginate_by = 20
    def get_queryset(self):
        images = Image.objects.all().order_by('pk')
        #if (not self.request.user.is_superuser):
        #    images = images.filter(user=self.request.user)
        return images


def home(request):
    """
    Home page.
    """
    return render(request, 'index.html')


def about(request):
    """
    Just an about page for guests
    """
    return render(request, 'about.html')