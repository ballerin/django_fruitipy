from django.contrib import admin

from api.models import Image
from api.views import Samples


# Register your models here.
admin.site.register(Image)
fields = ( 'image_tag', )
readonly_fields = ('image_tag',)
