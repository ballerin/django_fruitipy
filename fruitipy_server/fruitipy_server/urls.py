from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.conf.urls.static import static
from rest_framework import routers

from api import views as api_views
from website import views as website_views
from fruitipy_server import settings

router = routers.DefaultRouter()


urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^logout/$', auth_views.logout),
    url(r'^auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^accounts/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^accounts/profile/$', website_views.Profile),
    url(r'^get-token/$', api_views.GetToken.as_view(), name="get_token"),
    url(r'^samples/$', api_views.Samples.as_view(), name='samples'),
    url(r'^upload_photo/$', api_views.upload_photo.as_view(), name='new_photo'),
    url(r'^update_samples/$', api_views.UpdateSamples.as_view(), name='update_samples'),
    url(r'^delete_samples/(?P<pk>[0-9]+)/$', api_views.DeleteSamples.as_view(), name='delete_samples'),
    url(r'^delete_images/(?P<pk>[0-9]+)/$', api_views.DeleteImages.as_view(), name='delete_images'),
    url(r'^about/$', website_views.about, name='about'),
    url(r'^$', website_views.home, name='home'),
    url(r'^samples_list/$', website_views.SampleListView.as_view(), name='samples_list'),
    url(r'^images_list/$', website_views.ImageListView.as_view(), name='images_list'),
    url(r'^samples_list/(?P<req_id>[0-9]+)/$', website_views.HistoryListView.as_view(), name='history_list'),
    url(r'^spectrum_plot/([0-9]+)/$', website_views.spectrum_plot, name="spectrum_plot"),
    url(r'^', include(router.urls))
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
