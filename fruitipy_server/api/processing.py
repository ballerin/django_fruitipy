import os
import random
import json
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
from django.contrib.postgres.fields import JSONField
from api.serializers import SampleSerializer
from api.models import Sample
from rest_framework.renderers import JSONRenderer
from django.conf import settings


class Brix_nn(nn.Module):
    def __init__(self):
        super(Brix_nn, self).__init__()

        self.in_size = 278
        self.h1 = 9
        self.h2 = 8
        self.l1 = nn.Linear(self.in_size, self.h1)
        self.l2 = nn.Linear(self.h1, self.h2)
        self.l3 = nn.Linear(self.h2, 1)

    def forward(self, x):
        x = self.l1(x)
        x = F.relu(x)
        x = self.l2(x)
        x = F.relu(x)
        x = self.l3(x)
        x = F.relu(x)
        return x

def process(obj):
    '''
    if(obj.fruit == 0):
        obj.result = process_rasp(obj)
    elif(obj.fruit == 1):
        obj.result = process_grape(obj)
    else:
        print("ERROR")
        return "ERROR"
    '''
    obj.result = process_brix(obj)
    result = ""
    for key, value in obj.result.items():
        result += str(key) + ": " + str(value) + "\n"
    return obj, result

def process_rasp(obj):
    a = random.randint(0,1)
    if(a):
        return {"result":"Good"}
    else:
        return {"result":"Not Good"}


def correct_spectrum(data):
    dddata = np.diff(data, 2)

    w_ham = np.hamming(10)
    w_ham = w_ham / np.sum(w_ham)

    TH = 5  # tune!
    idx_drops = np.where(dddata >= TH)[0] + 1
    data_corrected = data.copy()

    for idx_drop in idx_drops:
        idx_prev = idx_drop - 1
        idx_post = idx_drop + 1
        data_curr = np.mean([data[idx_prev], data[idx_post]])
        data_corrected[idx_drop] = data_curr

    # smooth
    data_smoothed = np.convolve(data_corrected, w_ham, 'same')
    return data_smoothed

def process_brix(data):

    model = Brix_nn()
    model.load_state_dict(torch.load(os.path.join("/usr/src/app/fruitipy_server/models", "pytorch_model_state_dict.torch")))

    white = np.loadtxt(os.path.join("/usr/src/app/fruitipy_server/models","20170802_white_00000.csv"), skiprows=1)
    white = correct_spectrum(white)[5:-5]

    spectrum = correct_spectrum(np.array([float(i) for i in data.spectrum.split(',')][:288]))[5:-5]
    spectrum = np.lib.pad(spectrum, (278 - len(spectrum), 0), 'constant', constant_values=spectrum[0])

    spectrum = -np.log10(spectrum/white)

    brix = model(Variable(torch.from_numpy(spectrum.reshape(1, -1)).type(torch.FloatTensor)))
    print(brix)
    return {"brix":str(brix.data.numpy()[0][0]*34)}

def process_grape(data):
    acid = round(random.random(),4)
    brix = round(random.random(),4)
    ph = round(random.random(),4)
    return {"acid" : acid,
            "brix" : brix,
            "ph" : ph}
