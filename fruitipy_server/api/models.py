from django.db import models
from django.contrib.auth.models import User
from django.contrib import admin
from django.contrib.postgres.fields import JSONField
from django.utils.safestring import mark_safe

from api.choices import RIPENESS_CHOICES


class MlModel(models.Model):
    """
    Machine Learning models
    """
    val = models.TextField()

    def __string__(self):
        return self.path


class SpModel(models.Model):
    """
    Signal Processing algorithms
    """
    val = models.TextField()

    def __string__(self):
        return self.path


class Sample(models.Model):
    """
    Raspberry and Grape models
    """
    spectrum = models.CharField(max_length=2000)
    fruit = models.CharField(max_length=100)
    gps = models.TextField(null=True)
    lat = models.FloatField(null=True)
    lon = models.FloatField(null=True)
    date = models.DateTimeField()
    correct = models.IntegerField(default=-1, null=True)
    notes = models.TextField(null=True, max_length=1000)
    result = JSONField(default=None, null=True) #Results are stored here
    user = models.ForeignKey(User, editable=False, null=True)
    uni_id = models.IntegerField(default=-1, null=True)

    def __string__(self):
        return self.spectrum

    class Meta:
        ordering = ['-date']

class Image(models.Model):
    """
    Photos of the different fruits
    """
    fruit = models.CharField(max_length=100)
    photo = models.ImageField(upload_to='samples_photos')
    gps = models.TextField(null=True)
    date = models.DateTimeField(null=True)

    def __string__(self):
        return self.name

    def image_tag(self):
        return mark_safe('<img src="/directory/%s" width="150" height="150" />' % (self.image))

    image_tag.short_description = 'Image'