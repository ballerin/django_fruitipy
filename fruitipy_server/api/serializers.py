from rest_framework import serializers
from django.contrib.auth.models import User
from api import models


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'


class SampleSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Sample
        fields = '__all__'


class MlModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.MlModel
        fields = '__all__'


class SpModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.SpModel
        fields = '__all__'
