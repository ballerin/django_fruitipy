import pytz
import json
from bokeh.plotting import figure
from bokeh.embed import components
from bokeh.resources import CDN
from bokeh.embed import file_html
from django.contrib.auth.mixins import LoginRequiredMixin

from django.http import HttpResponse, HttpResponseRedirect
from django.http import JsonResponse
from django.views.generic import ListView
from rest_framework import status
from django.shortcuts import render
from django.views.generic import View
from django.contrib import messages
from rest_framework import viewsets
from rest_framework.authentication import TokenAuthentication, get_authorization_header, BasicAuthentication, SessionAuthentication

from rest_framework.authtoken.models import Token
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from api.models import MlModel, SpModel, Sample, Image
from api.choices import RIPENESS_LABELS
from api import serializers

from api.serializers import SampleSerializer

from api.processing import process


class Samples(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request, format=None):
        samples = Sample.objects.all()
        serializer = SampleSerializer(samples, many=True)
        return HttpResponse(serializer.data)

    def post(self, request, format=None):
        serializer = SampleSerializer(data=request.data)
        if serializer.is_valid():
            if(request.data['uni_id'] != '-1'):
                obj = Sample.objects.get(id=request.data['uni_id'])

                obj2 = Sample.objects.get(id=request.data['uni_id'])
                obj2.pk = None
                obj2.uni_id = obj.pk
                obj2.save()  # Saving old copy for history

                obj.notes = request.data['notes']
                obj.correct = request.data['correct']
            else:
                obj = serializer.save()
                obj.user = request.user

            obj, result = process(obj)
            obj.save()

            response_data = {}
            response_data['result'] = result
            response_data['uni_id'] = obj.id

            return HttpResponse(json.dumps(response_data), status=status.HTTP_201_CREATED)

        else:
            print(serializer.errors)
            return HttpResponse(status=status.HTTP_400_BAD_REQUEST)

class UpdateSamples(APIView):
    authentication_classes = (BasicAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, format=None):
        request_data = request.data.copy()
        serializer = SampleSerializer(data=request_data)

        if not serializer.is_valid():
            obj = Sample.objects.get(id=request_data['uni_id'])
            request_data['spectrum'] = obj.spectrum
            request_data['date'] = obj.date
            request_data['fruit'] = obj.fruit
            if request_data['notes'].replace(" ", "") == "":
                request_data['notes'] = None
            serializer = SampleSerializer(data=request_data)


        if serializer.is_valid():
            obj = Sample.objects.get(id=request_data['uni_id'])

            obj2 = Sample.objects.get(id=request_data['uni_id'])
            obj2.pk = None
            obj2.uni_id = obj.pk
            obj2.save()  # Saving old copy for history

            obj.notes = request_data['notes']
            if obj.notes == None:
                obj.notes = ""
            obj.notes = obj.notes.strip()
            obj.correct = request_data['correct']
            obj.gps = request_data['gps']

            obj, result = process(obj)
            obj.save()

            response_data = {}
            response_data['result'] = result
            response_data['uni_id'] = obj.id

            return HttpResponse(json.dumps(response_data), status=status.HTTP_200_OK)

        else:
            print(serializer.errors)
            return HttpResponse(status=status.HTTP_400_BAD_REQUEST)

class DeleteSamples(APIView):
    authentication_classes = (BasicAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, pk, format=None):
        snippet = Sample.objects.get(id=pk)
        snippet.delete()
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

class DeleteImages(APIView):
    authentication_classes = (BasicAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, pk, format=None):
        snippet = Image.objects.get(id=pk)
        snippet.delete()
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

class upload_photo(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, format=None):
        file = request.FILES.get('file', "NO FILE")
        if request.method == 'POST' and file != "NO FILE":
            instance = Image(photo=file, fruit="lel_test")
            instance.save()
            response_data = {}
            response_data['result'] = "OK"
            return HttpResponse(json.dumps(response_data),status=status.HTTP_201_CREATED)
        else:
            return HttpResponse(status=status.HTTP_400_BAD_REQUEST)



class GetToken(APIView):
    authentication_classes = (BasicAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, format=None):
        queryset = Token.objects.filter(user=request.user)
        token = ""

        if len(queryset) > 0:
            token = queryset[0]  # There is only one element in the queryset, and it is the token
            token.delete()

        token = Token.objects.create(user=request.user)
        json_token ={'token': pytz.unicode(token.key)}

        return JsonResponse(json_token)

