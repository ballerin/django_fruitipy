import pytest
from django.contrib.auth.models import User
from wheel.signatures import assertTrue


@pytest.mark.django_db
def test_new_user_works():
    new_user = User.objects.create_user(username='test', password='test')
    new_user.save()
