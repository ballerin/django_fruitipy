import pytest
from django.contrib.auth.models import User
from rest_framework.test import APIClient
from wheel.signatures import assertTrue
import base64


def make_auth():
    new_user = User.objects.create_user(username='test', password='test')
    new_user.save()

    c = APIClient()
    c.credentials(HTTP_AUTHORIZATION='Basic ' + base64.b64encode(bytes('test:test','utf-8')).decode('UTF-8'))
    response = c.post('/get-token/')
    return response


def get_token():
    return make_auth().data


@pytest.mark.django_db
def test_auth_for_token_works():
    get_token()


@pytest.mark.django_db
def test_request_new_sample_works():
    c = APIClient()
    sample_sent = {"spectrum": "1,2,3", "date": "2013-01-29T12:34:56.123Z", "fruit": 1, "result": "1"}
    c.credentials(HTTP_AUTHORIZATION='Token ' + get_token())
    response = c.post('/samples/', sample_sent)
    assertTrue(response.status_code == 201)


@pytest.mark.django_db
def test_request_fails_when_spectrum_is_missing():
    sample_sent = {"date": "2013-01-29T12:34:56.123Z", "fruit": 1, "result": "1"}
    c = APIClient()
    c.credentials(HTTP_AUTHORIZATION='Token ' + get_token())
    response = c.post('/samples/', sample_sent)
    assertTrue(response.status_code == 400)


@pytest.mark.django_db
def test_request_fails_when_date_is_missing():
    sample_sent = {"spectrum": "1,2,3", "fruit": 1, "result": "1"}
    c = APIClient()
    c.credentials(HTTP_AUTHORIZATION='Token ' + get_token())
    response = c.post('/samples/', sample_sent)
    assertTrue(response.status_code == 400)


@pytest.mark.django_db
def test_request_fails_when_fruit_is_missing():
    sample_sent = {"spectrum": "1,2,3", "date": "2013-01-29T12:34:56.123Z", "result": "1"}
    c = APIClient()
    c.credentials(HTTP_AUTHORIZATION='Token ' + get_token())
    response = c.post('/samples/', sample_sent)
    assertTrue(response.status_code == 400)


@pytest.mark.django_db
def test_request_fails_when_result_is_missing():
    sample_sent = {"spectrum": "1,2,3", "date": "2013-01-29T12:34:56.123Z", "fruit": 1}
    c = APIClient()
    c.credentials(HTTP_AUTHORIZATION='Token ' + get_token())
    response = c.post('/samples/', sample_sent)
    assertTrue(response.status_code == 400)