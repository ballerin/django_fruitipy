import pytest
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token


@pytest.mark.django_db
def test_token_works():
    new_user = User.objects.create_user(username='test', password='test')
    new_user.save()
    token = Token.objects.create(user=new_user)
