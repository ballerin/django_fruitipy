import pytest
from wheel.signatures import assertTrue
from api.serializers import SampleSerializer


@pytest.mark.django_db
def test_that_serialization_works():
    sample = {"spectrum": "1,2,3", "date": "2013-01-29T12:34:56.123Z", "fruit": 1, "result": 1}  # Only base fields
    serializer = SampleSerializer(data=sample)
    assertTrue(serializer.is_valid())


@pytest.mark.django_db
def test_that_serialization_savings_works():
    sample = {"spectrum": "1,2,3", "date": "2013-01-29T12:34:56.123Z", "fruit": 1, "result": "1"}
    serializer = SampleSerializer(data=sample)
    if serializer.is_valid():
        serializer.save()
    else:
        AssertionError()


@pytest.mark.django_db
def test_that_serialization_fails_when_spectrum_is_missing():
    sample = {"date": "2013-01-29T12:34:56.123Z", "fruit": 1, "result": "1"}
    serializer = SampleSerializer(data=sample)
    if serializer.is_valid():
        AssertionError()


@pytest.mark.django_db
def test_that_serialization_fails_when_date_is_missing():
    sample = {"spectrum": "1,2,3", "fruit": 1, "result": "1"}
    serializer = SampleSerializer(data=sample)
    if serializer.is_valid():
        AssertionError()


@pytest.mark.django_db
def test_that_serialization_fails_when_fruit_is_missing():
    sample = {"spectrum": "1,2,3", "date": "2013-01-29T12:34:56.123Z", "result": "1"}
    serializer = SampleSerializer(data=sample)
    if serializer.is_valid():
        AssertionError()


@pytest.mark.django_db
def test_that_serialization_fails_when_result_is_missing():
    sample = {"spectrum": "1,2,3", "date": "2013-01-29T12:34:56.123Z", "fruit": 1}
    serializer = SampleSerializer(data=sample)
    if serializer.is_valid():
        AssertionError()