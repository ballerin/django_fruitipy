#!/bin/bash

IMAGE=django

TAG=$1
if [ "$TAG" = "" ]; then
    TAG="test"
fi

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

echo "Building $IMAGE:$TAG"
#docker rmi $IMAGE:${TAG}
docker build --rm -t $IMAGE:${TAG} $DIR
