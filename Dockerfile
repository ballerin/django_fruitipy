FROM python:3
ENV PYTHONUNBUFFERED 1

WORKDIR /usr/src/app
ADD ./requirements.txt . 
RUN pip install --no-cache-dir -r requirements.txt
RUN pip install --no-cache-dir http://download.pytorch.org/whl/cpu/torch-0.4.0-cp36-cp36m-linux_x86_64.whl 
RUN pip install --no-cache-dir torchvision

ADD ./fruitipy_server ./fruitipy_server
ADD ./docker-entrypoint.sh ./docker-entrypoint.sh
# ENTRYPOINT [ "/bin/bash" ]
# CMD [ "python", "-u", "./your-daemon-or-script.py" ]
